<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::group(['middleware' => 'auth:api'], function () {

//	Route::name('sectors_path')->get('/sectores', 'Api\SectorController@index');
	Route::name('sector_store_path')->post('/sectores/crear', 'Api\SectorController@store');
	Route::name('zones_store_path')->post('/zonas/crear', 'Api\ZoneController@store');

	Route::post('/logout', 'Api\AuthController@logout');
	Route::name('colony_store_path')->post('/colonias/crear', 'Api\ColonyController@store');
});
Route::name('events_sector_path')->post('/eventos/sector', 'Api\EventController@getEventsFromSector');
Route::name('sectors_path')->get('/sectores', 'Api\SectorController@index');

Route::post('/register', 'Api\AuthController@register');
Route::post('/login', 'Api\AuthController@login');
//Route::name('sector_store_path')->post('/sectores/crear', 'Api\SectorController@store');
Route::name('zones_path')->get('/zonas', 'Api\ZoneController@index');
//Route::name('zones_store_path')->post('/zonas/crear', 'Api\ZoneController@store');
Route::name('colonies_path')->get('/colonias', 'Api\ColonyController@index');
Route::name('events_path')->get('/eventos', 'Api\EventController@index');
//Route::name('event_store_path')->post('/eventos/crear', 'Api\EventController@store');
Route::name('event_store_path')->post('/eventos/crear', 'Api\EventController@storeEvents');

Route::name('events_zone_path')->post('/eventos/zona', 'Api\EventController@getEventsFromZone');
Route::name('zones_sector_path')->post('/zonas/sector', 'Api\ZoneController@getZonesFromSector');

/*
Route::name('news_create_path')->get('/noticias/crear', 'NewsController@create');
Route::name('news_store_path')->post('/noticias/crear', 'NewsController@store');
Route::name('news_edit_path')->get('/noticias/{noticia}/editar', 'NewsController@edit');
Route::name('news_update_path')->put('/noticias/{noticia}', 'NewsController@Update');
Route::name('news_delete_path')->delete('/noticias/{noticia}', 'NewsController@delete');
 */
