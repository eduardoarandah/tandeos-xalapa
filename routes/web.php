<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('calendario');
});

Route::get('/login', function () {
    return view('auth');
});


Route::get('/calendario', function () {
    return view('calendario');
});

Route::get('/dashboard', function () {
    return view('index');
});

//Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');
