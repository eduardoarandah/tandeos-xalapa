<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Sector;
use App\Zone;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SectorController extends Controller
{
	public function index()
	{
		$sectors = Sector::all();

		return response()->json($sectors);
	}

	public function store(Request $request)
	{
		// dd($request->name);
		$data = [
			'status' => 'success',
			'desc' => 'Elemento Creado',
		];
		
		DB::beginTransaction();

		try {
			$sector = New Sector;

			$sector->name = $request->name;
			$sector->is_macrosector = $request->is_macrosector;	
			$sector->saveOrFail();

			if($request->is_macrosector)
			{
				$zone = new Zone;
				$zone->name = $request->name;
				$zone->sector_id = $sector->id;
				$zone->saveOrFail();

			}

			DB::commit();
			$data['sector'] = $sector;
		} 
		catch (\Exception $e) {
			DB::rollback();
			$data['status'] = 'error';
			$data['desc'] = 'Problema al realizar esta acción';
			$data['message'] = $e->getMessage();
		}
		
		return response()->json($data);
	}

	public function getEventsFromSector(Request $request)
	{
		$sector_id = $request->sector_id;
		$date = date("Y-m", strtotime($request->date));

		$events = Zone::Where('sector_id', '=', $sector_id)->events()->whereDate('date_start', '>=', $date)->get();

		return response()->json($events);


	}



	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  \App\Sector  $sector
	 * @return \Illuminate\Http\Response
	 */
	public function edit(Sector $sector)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \App\Sector  $sector
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, Sector $sector)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  \App\Sector  $sector
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(Sector $sector)
	{
		//
	}
}
