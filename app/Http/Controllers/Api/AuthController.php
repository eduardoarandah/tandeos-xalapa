<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Validator;

class AuthController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	private $apiToken;
	public function __construct()
	{
		$this->apiToken = uniqid(base64_encode(Str::random(40)));
	}
	public function index()
	{
		//
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function login (Request $request)
	{
		$credentials = $request->only('email', 'password');
		$email = $request->email;
		$user = null;
		$status = 'error';
		$desc = 'Error de acceso';
		if (Auth::attempt($credentials)) {
			// Authentication passed...
			$user = User::where('email', $email)->get()->first();
			// dd($user);

			$token = Str::random(60);


			// dd($user);
			// $request->user()->forceFill([
			//     'api_token' => hash('sha256', $token),
			// ])->save();
			$user->forceFill([
				'api_token' => hash('sha256', $token),
			])->save();
			$auth = Auth::guard('api');
			$auth = Auth::login($user);
			$user = Auth::user();
			$status = 'success';
			$desc = 'Usuario loggeado';

		}
		return response()->json([
			'user' => $user,
			'status' => $status,
			'desc' => $desc
		]);

	}
	public function register(Request $request) 
	{
		$status = true;
		$desc = 'Usuario registrado';
		// return response()->json($request);
		$token = Str::random(60);
		$validator = Validator::make($request->all(), [ 
			'name' => 'required', 
			'email' => 'required|email', 
			'password' => 'required', 
			'c_password' => 'required|same:password', 
		]);
		if ($validator->fails()) { 
			return response()->json(['error'=>$validator->errors()]);
		}
		$user = User::create([
			'name' => $request->name,
			'email' => $request->email,
			'password' => Hash::make($request->password),
			'api_token' => hash('sha256', $token),
		]);
		$user->forceFill([
			'api_token' => hash('sha256', $token),
		])->save();
		return response()->json([
			'user' => $user,
			'status' => $status,
			'desc' => $desc
		]);
	}

	public function logout(Request $request){
		$user = Auth::user();
		$status = 'error';
		$desc = 'problema al cerrar sesión';
		if($user){
			// $this->guard()->logout();
			// dd($request);
			// return response()->json(['user' => $user->session()]);
			// $request->session()->invalidate();
			$user->forceFill([
				'api_token' => $token = Str::random(60),
			])->save();
			// $user = Auth::logout();
			// dd($user);
			$status = 'success';
			$desc = 'Usuario desloggeado';
			// $user = LoginController::logout();
		}
		return response()->json([
			'user' => $user,
			'status' => $status,
			'desc' => $desc
		]);
	}
	/**
	 * Display the specified resource.
	 *
	 * @param  \App\User  $user
	 * @return \Illuminate\Http\Response
	 */
	public function show(User $user)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  \App\User  $user
	 * @return \Illuminate\Http\Response
	 */
	public function edit(User $user)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \App\User  $user
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, User $user)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  \App\User  $user
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(User $user)
	{
		//
	}
}
